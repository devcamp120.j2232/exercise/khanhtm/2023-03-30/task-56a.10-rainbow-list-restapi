package com.example.demo.RainbowRestAPI;

public class Rainbow {
    
    String color ;

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    
    public Rainbow(){

    }
    public Rainbow(String color){
        this.color = color ;
    }
  
}
